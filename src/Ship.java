import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by maksi on 29.09.2017.
 */
public class Ship {
    private int[] location;

    @Override
    public String toString() {
        return "Ship{" +
                "location=" + Arrays.toString(location) +
                '}';
    }

    public void setLocation(int[] location) {

        this.location = location;
    }

    Scanner scanner = new Scanner(System.in);

    public void shots(int x) {
        int y = 0;
        while (true) {
            int shot;
            shot = scanner.nextInt();
            if (shot == x || shot == x + 1 || shot == x + 2) {
                System.out.println("Попал");

                if (y == 2) {
                    break;
                }
                y++;
            } else {
                {
                    System.out.println("Мимо");
                }
            }
        }
    }
}